package pl.kasprowski.springgurureactivewebfluxclient.service;

import org.springframework.context.ApplicationListener;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.stereotype.Service;
import pl.kasprowski.springgurureactivewebfluxclient.client.StockQuoteClient;
import pl.kasprowski.springgurureactivewebfluxclient.repository.QuoteRepository;

@Service
public class QuoteMonitorService implements ApplicationListener<ContextRefreshedEvent> {

    private final StockQuoteClient client;
    private final QuoteRepository repository;

    public QuoteMonitorService(final StockQuoteClient client, final QuoteRepository repository) {
        this.client = client;
        this.repository = repository;
    }

    @Override
    public void onApplicationEvent(ContextRefreshedEvent ignore) {
        client.getQuoteStream()
                .log("quote-monitor-service")
                .take(10)
                .subscribe(quote -> repository.save(quote)
                        .subscribe(savedQuote -> System.out.println("################# Saved quote:" + savedQuote.getId())));
    }
}
