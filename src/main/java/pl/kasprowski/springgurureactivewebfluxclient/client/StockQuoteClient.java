package pl.kasprowski.springgurureactivewebfluxclient.client;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Service;
import org.springframework.web.reactive.function.client.WebClient;
import pl.kasprowski.springgurureactivewebfluxclient.configuration.ServiceConfigurationProperties;
import pl.kasprowski.springgurureactivewebfluxclient.domain.Quote;
import reactor.core.publisher.Flux;

@Slf4j
@Service
public class StockQuoteClient {

    private final ServiceConfigurationProperties properties;
    private final String baseUrl;

    @Autowired
    public StockQuoteClient(final ServiceConfigurationProperties properties) {
        this.properties = properties;
        this.baseUrl = String.format("http://%s:%s", properties.getHost(), properties.getPort());
    }

    public Flux<Quote> getQuoteStream() {
        return WebClient.builder()
                .baseUrl(baseUrl)
                .build()
                .get()
                .uri(properties.getPath())
                .accept(MediaType.APPLICATION_STREAM_JSON)
                .retrieve()
                .bodyToFlux(Quote.class);
    }
}
