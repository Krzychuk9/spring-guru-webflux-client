package pl.kasprowski.springgurureactivewebfluxclient.configuration;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

@Data
@Component
@ConfigurationProperties("service")
public class ServiceConfigurationProperties {
    private String host;
    private String port;
    private String path;
}
