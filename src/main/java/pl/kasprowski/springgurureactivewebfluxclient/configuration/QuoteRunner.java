package pl.kasprowski.springgurureactivewebfluxclient.configuration;

import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;
import pl.kasprowski.springgurureactivewebfluxclient.repository.QuoteRepository;
import reactor.core.Disposable;

@Component
public class QuoteRunner implements CommandLineRunner {

    private final QuoteRepository repository;

    public QuoteRunner(QuoteRepository repository) {
        this.repository = repository;
    }

    @Override
    public void run(String... args) {
        final Disposable disposable = repository.findWithTailableCursorBy()
                .subscribe(quote -> System.out.println("#############################" + quote.getId()));
        disposable.dispose();
    }
}
