package pl.kasprowski.springgurureactivewebfluxclient.repository;

import org.springframework.data.mongodb.repository.ReactiveMongoRepository;
import org.springframework.data.mongodb.repository.Tailable;
import org.springframework.stereotype.Repository;
import pl.kasprowski.springgurureactivewebfluxclient.domain.Quote;
import reactor.core.publisher.Flux;

@Repository
public interface QuoteRepository extends ReactiveMongoRepository<Quote, String> {

    @Tailable
    Flux<Quote> findWithTailableCursorBy();
}
