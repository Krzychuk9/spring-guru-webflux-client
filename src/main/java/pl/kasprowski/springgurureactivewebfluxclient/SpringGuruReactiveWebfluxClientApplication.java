package pl.kasprowski.springgurureactivewebfluxclient;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringGuruReactiveWebfluxClientApplication {

    public static void main(String[] args) {
        SpringApplication.run(SpringGuruReactiveWebfluxClientApplication.class, args);
    }
}
